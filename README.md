# Vssl: Distributed computing made easy with free monads

[![pipeline status](https://gitlab.com/benweitzman/polysemy-distributed/badges/master/pipeline.svg)](https://gitlab.com/benweitzman/polysemy-distributed/commits/master)
[![coverage report](https://gitlab.com/benweitzman/polysemy-distributed/badges/master/coverage.svg)](https://benweitzman.gitlab.io/polysemy-distributed)

#### Example

`Server code`
```haskell
main = hostDistributedEffect 8080 @'[YourCustomEffect] $ runM
    . runYourCustomEffect
```

`Client code`
```haskell
runClient = runM
    . raiseClientExcpetions
    . runHTTP
    . runDistributedEffect @YourCustomEffect "http://localhost:8080"
```


## Background

#### What are free monads 
Free monads are powerful tools for writing complex software because
they let us define inert, composable interfaces for effectful
computations and _seperate_ implementations that can fulfil those
interfaces.

For any given effect interface, you can write many different
implementations. Perhaps a pure implementation for tests and a
side-effectful production implementation. There's a lot of flexiblity
in how an implementation will fulfill an interface. In theory, calls
to an effectful interface don't even have to be run on the same
machine as their implementation. And in practice, Vssl provides
the machinery for making distributed computing with free monads easy.

#### Distributed context aware effects

In addition to the primary effect data, secondary information from
other effects can also be included in requests and read from
responses (via HTTP headers). There are two motivating examples of
secondary effects: distributed tracing and cache dependencies.

* In a distributed computing environment, it's often useful to use
some sort of tracing framework for collecting information about how
requests cascade through a system. There a few different frameworks
out there, but in most, it's necessary to send some sort of "trace"
identifier along with a request so that different services can report
profiling statistics associated with that common trace. This is an
example of a secondary effect which would need to send additional data
(i.e. the trace id) along with a request

* In an environment with aggresive caching, cache invalidation can
be a big headache. One approach to dealing with cache invalidation
might be to keep track of dependencies between cache keys. If we know
that cache key "A" depends on cache key "B", then if we invalidate or
otherwise change the contents of "B", then we know we have to
invalidate "A" as well. An secondary effect that is managing a cache
can inject information about cache keys it used into the response of a
primary effect so that clients can have some awareness of the
distributed cache dependencies.

#### Protocol

Communication between nodes is handled over HTTP, with all data being
encoded with JSON. All effects are called/handled from a single POST
endpoint (n.b. future versions of this library may add support for
more advanced HTTP routing). Servers can implement one effect or many
effects, and can use other effects in their implementations which will
not be exposed over the network.

This approach to distributed computing will certainly work best when
you are writing both the client and server with this library, though
if you wanted to, there's certainly no reason why you couldn't write
your client + server in, say, different languages. That said, because
this libraries communicates over a rigid (and fairly arbitrary)
protocol, in order to make use of this library, you'll either need to
control both the client + server or get quite lucky and have whatever
is out of your control match the protocol exactly.
