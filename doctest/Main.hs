import           System.Environment
import           Test.DocTest

main = doctest
  [ "-isrc/"
  , "--fast"
  , "-XGADTs"
  , "-XTypeFamilies"
  , "-XConstraintKinds"
  , "-XDefaultSignatures"
  , "-XMultiParamTypeClasses"
  , "-XPartialTypeSignatures"
  , "-XTypeOperators"
  , "-XDataKinds"
  , "-XPolyKinds"
  , "-XRankNTypes"
  , "-XStandaloneDeriving"
  , "-XScopedTypeVariables"
  , "-XRecordWildCards"
  , "-XTypeApplications"
  , "-XFlexibleInstances"
  , "-XQuantifiedConstraints"
  , "-XFlexibleContexts"
  , "-XOverloadedStrings"
  , "-XLambdaCase"
  , "-XTupleSections"
  , "-XBangPatterns"
  , "-XDeriveAnyClass"
  , "-XDerivingStrategies"

  , "src/Polysemy/Observe.hs"
  , "src/Polysemy/Distributed/Client.hs"
  , "src/Polysemy/Distributed/Server.hs"
  ]
