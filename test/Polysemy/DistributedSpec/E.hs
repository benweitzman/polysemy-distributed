{-# LANGUAGE TemplateHaskell #-}

module Polysemy.DistributedSpec.E where

import           Data.Aeson
import           Polysemy
import           Polysemy.Distributed
import Polysemy.State

data Asymmetric = Asymmetric deriving (Show, Eq)

instance ToJSON Asymmetric where
  toJSON _ = ""

instance FromJSON Asymmetric where
  parseJSON = withObject "Asymmetric" $ \o -> return Asymmetric

data E (m :: * -> *) a where
  DoAThing :: Int -> E m Bool
  AsymmetricResponse :: E m Asymmetric

makeSem ''E

runE :: Sem (E ': r) a -> Sem r a
runE = interpret $ \case
  DoAThing n -> return $ even n
  AsymmetricResponse -> return Asymmetric

deriving instance Show (E m a)

instance ToJSON (E m a) where
  toJSON (DoAThing s) = object
    ["tag" .= ("DoAThing" :: String)
    ,"contents" .= s
    ]
  toJSON AsymmetricResponse = object
    ["tag" .= ("AsymmetricResponse" :: String)
    ]

instance TransJSON E where
  transFrom DoAThing{} = id
  transFrom AsymmetricResponse{} = id

  transTo DoAThing{} = id
  transTo AsymmetricResponse{} = id

instance ParseEffect E where
  effectParsers =
    [ ExistentialParser $ withObject "DoAThing" $ \o -> do
        ("DoAThing" :: String) <- o .: "tag"
        DoAThing <$> o .: "contents"
    , ExistentialParser $ withObject "AsymmetricResponse" $ \o -> do
        ("AsymmetricResponse" :: String) <- o .: "tag"
        return AsymmetricResponse
    ]

instance ToJSON s => ToJSON (State s m a) where
  toJSON Get = object
    ["tag" .= ("Get" :: String)
    ]
  toJSON (Put s) = object
    ["tag" .= ("Put" :: String)
    ,"contents" .= s
    ]

instance (ToJSON s, FromJSON s) => TransJSON (State s) where
  transFrom Get = id
  transFrom Put{} = id

  transTo Get = id
  transTo Put{} = id

instance FromJSON s => ParseEffect (State s) where
  effectParsers =
    [ ExistentialParser $ withObject "Get" $ \o -> do
        ("Get" :: String) <- o .: "tag"
        return Get
    , ExistentialParser $ withObject "Put" $ \o -> do
        ("Put" :: String) <- o .: "tag"
        Put <$> o .: "contents"
    ]
