{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TemplateHaskell #-}

module Polysemy.DistributedSpec.Trace where

import Polysemy.Alter
import           Polysemy
import Polysemy.State
import Polysemy.Reader
import Polysemy.Output
import Polysemy.Internal
import Polysemy.Internal.CustomErrors
import Polysemy.Distributed.Vessel
import Unsafe.Coerce

data Trace m a where
  Trace :: String -> m a -> Trace m a

makeSem ''Trace

instance VesselAccess Trace where
  data instance Key Trace s a where
    TraceId :: Key Trace "X-Trace-Id" Int
    ParentSpanId :: Key Trace "X-Parent-Span-Id" (Maybe Int)


data TraceInfo = TraceInfo
  { traceId :: Int
  , parentSpanId :: Maybe Int
  }

data Span = Span
  { spanTraceId :: Int
  , spanParent :: Maybe Int
  , spanId :: Int
  , spanData :: String
  } deriving (Show, Eq, Ord)

distributedRootTraceProvider
  :: Member (Query (Vessel Client)) r
  => Sem (Reader TraceInfo ': r) a
  -> Sem r a
distributedRootTraceProvider sem = do
  s <- query
  let Just traceId = find TraceId s
      Just parentSpanId = find ParentSpanId s
  runReader TraceInfo{..} sem

liftTraceDistributed
  :: Members '[Alter (Deposit Client), Reader TraceInfo] r
  => Sem r a
  -> Sem r a
liftTraceDistributed = interceptH @(Reader TraceInfo) $ \case
  Ask -> do
    x <- ask @TraceInfo
    pureT x
  Local f e -> do
    m <- runT e
    i@TraceInfo{..} <- f <$> ask
    r <- raise $ runReader i $
      alter (Deposit ParentSpanId parentSpanId) $
        alter (Deposit TraceId traceId) m
    return r

runTrace
  :: (Members '[State Int, Reader TraceInfo, Output Span] r)
  => Sem (Trace ': r) a
  -> Sem r a
runTrace = interpretH $ \case
  Trace spanData m -> do
    spanId <- get
    modify @Int (+1)
    TraceInfo{..} <- ask
    output Span
      { spanParent = parentSpanId
      , spanTraceId = traceId
      , ..
      }
    h <- runT m
    res <- raise . runTrace . local (\s -> s { parentSpanId = Just spanId }) $
      h
    return res

runDistributedTrace
  :: Members '[State Int, Reader TraceInfo, Output Span, Alter (Deposit Client)] r
  => Sem (Trace ': r) a
  -> Sem r a
runDistributedTrace = liftTraceDistributed . runTrace

localRootTraceProvider
  :: Members '[State Int] r
  => Sem (Reader TraceInfo ': r) e
  -> Sem r e
localRootTraceProvider e = do
  traceId <- get
  flip runReader e TraceInfo
    { traceId = traceId
    , parentSpanId = Nothing
    }

addTrace
  :: forall e r a .
     (Members '[e, Trace] r
     ,FirstOrder e "intercept"
     )
  => (forall m x . e m x -> String)
  -> Sem r a
  -> Sem r a
addTrace f = intercept @e $ \e ->
  let e' :: e (Sem r) x
      e' = unsafeCoerce e
  in trace (f e) $ send e'
