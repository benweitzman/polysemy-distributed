{-# LANGUAGE TemplateHaskell #-}

module Polysemy.DistributedSpec.Cache where

import Polysemy
import Polysemy.Internal
import Polysemy.Distributed.Vessel
import Polysemy.Alter
import Polysemy.Output
import Polysemy.Observe
import Polysemy.Writer

data Cache m a where
  Cache :: String -> m a -> Cache m a

makeSem ''Cache

instance VesselAccess Cache where
  data instance Key Cache s a where
    CacheDependencies :: Key Cache "X-Cache-Dependencies" [String]

runVesselCacheClient
  :: Member (Observe (Vessel Server)) r
  => Sem (Cache ': r) a
  -> Sem r ([String], a)
runVesselCacheClient =
  eliminateDownstreamDeposit
  . runVesselCache
  . raiseUnder

runVesselCacheServer
  :: Member (Output (Deposit Server)) r
  => Sem (Cache ': r) a
  -> Sem r ([String], a)
runVesselCacheServer =
  eliminateUpstreamVessel
  . runVesselCache
  . raiseUnder

runVesselCache
  :: Members '[Observe (Vessel Server), Output (Deposit Server)] r
  => Sem (Cache ': r) a
  -> Sem r ([String], a)
runVesselCache = returnVesselCache
  . runOutputMonoid @[String] id
  . observeVesselCache
  . runCache'
  . raiseUnder @(Output [String])


returnVesselCache
  :: Members '[Output (Deposit Server)] r
  => Sem r ([String], a)
  -> Sem r ([String], a)
returnVesselCache m = do
  (deps, res) <- m
  output $ Deposit CacheDependencies deps
  return (deps, res)

runCache
  :: Sem (Cache ': r) a
  -> Sem r ([String], a)
runCache m = runOutputMonoid id $ runCache' (raiseUnder @(Output [String]) m)

observeVesselCache
  :: Members '[Observe (Vessel Server), Output [String]] r
  => Sem r a
  -> Sem r a
observeVesselCache m = do
  (s, x) <- observe @(Vessel Server) m
  let remoteDeps = case find CacheDependencies s of
        Nothing -> []
        Just xs -> xs
  output remoteDeps
  return x

runCache'
  :: Member (Output [String]) r
  => Sem (Cache ': r) a
  -> Sem r a
runCache' = interpretH $ \case
  Cache key m -> do
    m' <- runT m
    x <- raise $ runCache' m'
    output [key]
    return x
