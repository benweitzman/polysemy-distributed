{-# LANGUAGE TemplateHaskell #-}

module Polysemy.DistributedSpec where

import qualified Data.Set as S
import           Data.Aeson
import qualified Network.HTTP.Client        as H
import           Network.HTTP.Types hiding (Query)
import           Network.Wai
import           Polysemy
import Polysemy.Distributed.Vessel
import Polysemy.State
import Polysemy.Reader
import Polysemy.Output
import Polysemy.Writer
import Polysemy.Observe
import Polysemy.Embed
import Polysemy.Error
import Polysemy.Alter
import           Polysemy.Distributed
import           Polysemy.DistributedSpec.E
import           Polysemy.DistributedSpec.Trace
import           Polysemy.DistributedSpec.Cache
import           Polysemy.HTTP
import           Test.Hspec
import Data.IORef
import Control.Exception (try)

handle f e = runError e >>= \case
  Right x -> return x
  Left y -> f y

spec :: Spec
spec = do
  describe "Polysemy.Distributed" $ do
    it "should round trip" $ do
      let hostApp = distributedEffectApplication @'[E] (runM . runE)

          run = runM
            . handle (error . show)
            . applicationHTTP hostApp
            . runDistributedEffect @E "http://localhost:80"

      res <- run $ do
        doAThing 34

      res `shouldBe` True

    it "can host multiple effects" $ do
      ref <- newIORef 0
      let hostApp = distributedEffectApplication @'[E, State Int] $
            runM
            . runE
            -- State is somewhat funny effect to interpret over the network
            -- like this. Using `evalState` wouldn't work as naively expected,
            -- since each effect stack is run down to IO for every request, and
            -- thus no pure state is maintained between calls. Using an IORef
            -- keeps state saved inbetween calls, leading to the expected behavior.
            . runStateIORef @Int ref


          run = runM
            . handle (error . show)
            . applicationHTTP hostApp
            . runDistributedEffect @E "http://localhost:80"
            . runDistributedEffect @(State Int) "http://localhost:80"

      res <- run $ do
        s <- get @Int
        e <- doAThing 34
        put (s + 1)
        s' <- get @Int
        return (s', e)

      res `shouldBe` (1, True)

    it "can deal with an error" $ do
      let hostApp = distributedEffectApplication @'[E] (fmap undefined . runM . runE)

          run = runM
            . handle (return . Left)
            . fmap Right
            . applicationHTTP hostApp
            . runDistributedEffect @E "http://localhost:80"

      res <- run $ do
        doAThing 34

      res `shouldBe` Left BadResponse

    it "can deal with bad json" $ do
      let hostApp = distributedEffectApplication @'[E] (runM . runE)

          run = runM
            . handle (return . Left)
            . fmap Right
            . applicationHTTP hostApp
            . runDistributedEffect @E "http://localhost:80"

      res <- run asymmetricResponse

      res `shouldBe` Left InvalidParse

    it "can raise an exception if you ask it to" $ do
      let hostApp = distributedEffectApplication @'[E] (runM . runE)

          run = runM
            . raiseClientExceptions
            . applicationHTTP hostApp
            . runDistributedEffect @E "http://localhost:80"

      res <- try $ run asymmetricResponse

      res `shouldBe` Left InvalidParse

    it "can carry cache information" $ do
      let hostApp = distributedVesselEffectApplication @'[E] $
            fmap snd
            . runVesselCacheServer
            . (\e -> cache "server" $ runE e)

          run = runM @IO
            . handle (error . show)
            . applicationHTTP hostApp
            . shareVessel
            . runVesselCacheClient
            . runDistributedVesselEffect @E "http://localhost:80"

      (deps, res) <- run $ cache "client" $ do
        doAThing 34

      S.fromList deps `shouldBe` S.fromList ["client", "server"]

    it "can carry trace information" $ do
      ref <- newIORef 0
      hostTraceRef <- newIORef []

      let eTracer :: E m x -> String
          eTracer (DoAThing x) = "DoAThing " ++ show x

      let recordHostTrace :: Member (Embed IO) r => Sem r ([Span], a) -> Sem r a
          recordHostTrace e = do
            (o, a) <- e
            embed $ modifyIORef hostTraceRef (o ++)
            return a

      let hostApp = distributedVesselEffectApplication @'[E] $
            recordHostTrace
            . runStateIORef @Int ref
            . runOutputList @Span
            . distributedRootTraceProvider
            . runTrace
            . runE
            . addTrace eTracer

          run = runM
            . runOutputList @Span    --}
            . runStateIORef ref      --} -- trace implementation
            . localRootTraceProvider --}

            . shareVessel
            . runDistributedTrace
            . handle (error . show)
            . applicationHTTP hostApp
            . runDistributedVesselEffect @E "http://localhost:80"
            . addTrace eTracer

          f = do
            doAThing 34
            doAThing 52
      (traceLog1, res1) <- run f
      (traceLog2, res2) <- run f
      hostTraces <- readIORef hostTraceRef
      let allTraces = S.fromList $ traceLog1 ++ traceLog2 ++ hostTraces
      allTraces `shouldBe` S.fromList
        [Span -- The first span starts on the client
          {spanTraceId = 0, spanParent = Nothing, spanId = 0, spanData = "DoAThing 34"}
        ,Span -- On the server, with the parent id
          {spanTraceId = 0, spanParent = Just 0,  spanId = 1, spanData = "DoAThing 34"}
        ,Span -- Back to the client, a new span in the same trace
          {spanTraceId = 0, spanParent = Nothing, spanId = 2, spanData = "DoAThing 52"}
        ,Span -- Second span on the server
          {spanTraceId = 0, spanParent = Just 2,  spanId = 3, spanData = "DoAThing 52"}
        ,Span -- Second trace
          {spanTraceId = 4, spanParent = Nothing, spanId = 4, spanData = "DoAThing 34"}
        ,Span
          {spanTraceId = 4, spanParent = Just 4,  spanId = 5, spanData = "DoAThing 34"}
        ,Span
          {spanTraceId = 4, spanParent = Nothing, spanId = 6, spanData = "DoAThing 52"}
        ,Span
          {spanTraceId = 4, spanParent = Just 6,  spanId = 7, spanData = "DoAThing 52"}
        ]
