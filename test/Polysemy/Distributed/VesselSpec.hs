module Polysemy.Distributed.VesselSpec where

import           Data.Aeson
import           Data.IORef
import qualified Network.HTTP.Client            as H
import           Network.HTTP.Types
import           Network.Wai
import           Polysemy
import           Polysemy.Distributed.Vessel
import           Polysemy.HTTP
import           Polysemy.State
import qualified Data.ByteString.Lazy as LBS
import           Test.Hspec
import Polysemy.Alter

spec :: Spec
spec = do
  describe "Vessel" $ do
    it "should round trip" $ do
      let res = run . shareVessel $ do
            s <- query
            let initial = find EHeader s
            alter (Deposit EHeader 10) $ do
--              alter (fromHeaders . toHeaders) $ do
                s'' <- query
                let second = find EHeader s''
                return (initial, second)
      res `shouldBe` (Nothing, Just 10)

    it "should keep seperate scopes seperate" $ do
      let res = run . shareVessel $ do
            s <- query
            let initial = find EHeader s
            first <- alter (Deposit EHeader 10) $ do
              s' <- query
              return $ find EHeader s'
            s' <- query
            let afterFirst = find EHeader s'
            return (initial, first, afterFirst)
      res `shouldBe` (Nothing, Just 10, Nothing)

    it "should work with multiple headers" $ do
      let res = run . shareVessel $ do
            alter (Deposit EHeader 10) $ do
              alter (Deposit FHeader1 True) $ do
                s <- query
                let eHeader = find EHeader s
                    fHeader1 = find FHeader1 s
                return (eHeader, fHeader1)
      res `shouldBe` (Just 10, Just True)

    it "converts to headers" $ do
      let res = run . shareVessel $ do
            alter (Deposit EHeader 10) $ do
              alter (Deposit FHeader1 True) $ do
                s <- query @(Vessel Client)
                return s
      toHeaders res `shouldBe`
        [("Header1", "true")
        ,("Header", "10")
        ]

data E m a where
  X :: E m Bool

instance VesselAccess E where
  data instance Key E s a where
    EHeader :: Key E "Header" Int

data F m a where
  Y :: F m Bool

instance VesselAccess F where
  data instance Key F s a where
    FHeader1 :: Key F "Header1" Bool
    FHeader2 :: Key F "Header2" String
