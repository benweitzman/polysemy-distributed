module Polysemy.HTTPSpec where

import qualified Network.HTTP.Client as H
import           Network.HTTP.Types
import           Network.Wai
import           Polysemy
import           Polysemy.HTTP
import           Test.Hspec

spec :: Spec
spec = do
  describe "applicationHTTP" $ do
    it "should send a body" $ do
      let app :: Application
          app req respond = respond $ responseLBS ok200 [] "hello"

      resp <- runM $ applicationHTTP app $ performRequest H.defaultRequest
      H.responseBody resp `shouldBe` "hello"

    it "should send a status" $ do
      let app :: Application
          app req respond = respond $ responseLBS ok200 [] "hello"

      resp <- runM $ applicationHTTP app $ performRequest H.defaultRequest
      H.responseStatus resp `shouldBe` ok200

    it "should react to input" $ do
      let app :: Application
          app req respond = do
            body <- lazyRequestBody req
            respond $ responseLBS ok200 [] $ body

      resp <- runM $ applicationHTTP app $ performRequest H.defaultRequest
        { H.requestBody = H.RequestBodyBS "aha!"
        }
      H.responseBody resp `shouldBe` "aha!"
