{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Polysemy.HTTP.Convert
  (convertRequest
  ,convertResponse
  ) where

import           Control.Concurrent.Chan
import           Control.Monad
import           Control.Monad.ST
import qualified Data.ByteString              as B
import qualified Data.ByteString.Builder      as B
import           Data.ByteString.Lazy         (ByteString)
import qualified Data.ByteString.Lazy         as LB
import           Data.STRef
import qualified Data.Vault.Lazy              as V
import qualified Network.HTTP.Client          as H
import qualified Network.HTTP.Client.Internal as H
import qualified Network.HTTP.Types           as H
import           Network.Socket
import qualified Network.Wai                  as W
import qualified Network.Wai.Internal         as W

convertRequest :: H.Request -> IO W.Request
convertRequest req = do
  chan <- newChan

  -- The WAI Request field `requestBody` is actually
  -- an IO action that return a /chunk/ of the body. It will
  -- be called multiple times until it returns an empty string.
  case H.requestBody req of
    H.RequestBodyBS body  -> sendInChan chan body
    H.RequestBodyLBS body -> sendInChan chan $ LB.toStrict body

  return W.defaultRequest
    { W.requestMethod = H.method req
    , W.httpVersion = H.requestVersion req
    -- rawPathInfo
    , W.rawQueryString = H.queryString req
    , W.requestHeaders = H.requestHeaders req
    , W.isSecure = H.secure req
    , W.remoteHost = SockAddrInet 80 $ tupleToHostAddress (127, 0, 0, 1)
    , W.queryString = H.parseQuery $ H.queryString req
    , W.pathInfo = H.decodePathSegments $ H.path req
    , W.requestBody = readChan chan
    , W.vault = V.empty
    , W.requestBodyLength = case H.requestBody req of
        H.RequestBodyBS body -> W.KnownLength . fromIntegral $ B.length body
    }

sendInChan chan x = do
  writeChan chan x
  writeChan chan ""

convertResponse :: W.Response -> H.Response ByteString
convertResponse resp = H.Response
  { H.responseStatus = W.responseStatus resp
  , H.responseVersion = H.http11
  , H.responseHeaders = W.responseHeaders resp
  , H.responseClose' = H.ResponseClose $ return ()
  , H.responseCookieJar = H.CJ []
  , H.responseBody = case resp of
      W.ResponseBuilder _ _ builder -> B.toLazyByteString builder
  }
