{-# LANGUAGE AllowAmbiguousTypes #-}
{-|
This module contains functions that create /servers/ which can
respond to requests to handle effects.
-}

module Polysemy.Distributed.Server where

import Control.Monad
import           Data.Aeson
import           Network.HTTP.Types (status200, status500)
import Control.Exception
import           Network.Wai
import qualified Network.Wai.Handler.Warp as W
import           Polysemy
import           Polysemy.Distributed.Types
import           Polysemy.Internal
import Polysemy.Reader
import Polysemy.Distributed.Vessel
import Polysemy.Output
import Polysemy.Alter

-- | Run a server that will handle requests to interpret effects.
-- Several effects can interpreted simultaneously, though the effects
-- that are being interpreted must be explicitly annoted with a type
-- application.
--
-- Effects are parsed from JSON using instances of the typeclass
-- `ParseEffect` @e@
hostDistributedEffect
  :: forall es r.
     (Members es r, FromJSON (OneOf r es))
  => Int -- ^ Port
  -> (forall a . Sem r a -> IO a) -- ^ Effect interpreter
  -> IO ()
hostDistributedEffect port runE = W.run port $
  distributedEffectApplication @es runE

type DistributedServerCapabilities =
  '[Query (Vessel Client) -- read data from clients
   ,Output (Deposit Server) -- send data from server
   ,Embed IO
   ]

-- | Run a server that will handle requests to interpret effects.
-- Several effects can interpreted simultaneously, though the effects
-- that are being interpreted must be explicitly annoted with a type
-- application.
--
-- Effects are parsed from JSON using instances of the typeclass
-- `ParseEffect` @e@
--
-- This version of the function gives access to the `Vessel`, which
-- allows effects other than `es` to communicate with other over the
-- network via HTTP headers. This function will likely pair with
-- `runDistributedEffect` on the client.
--
-- Instead of requiring the effect interpreter to handle everything
-- down to `IO` like in the simple version, we require the interpreter
-- to handle everything /except/ `Query` `Vessel`, `Output`
-- `Deposit`, and the final `Embed IO`.
--
-- * `Query` `Vessel` can be used by effects to read from the
-- /request/ headers.
-- * `Output` `Deposit` can be used to write to the /response/
-- headers.
hostDistributedVesselEffect
  :: forall es r.
     (Members es r, FromJSON (OneOf r es))
  => Int
  -- ^ Port
  -> (forall a . Sem r a -> Sem DistributedServerCapabilities a)
  -- ^ Effect interpreter
  -> IO ()
hostDistributedVesselEffect port runE = W.run port $
  distributedVesselEffectApplication @es runE



-- | Like `hostDistributedVesselEffect` except it returns an
-- `Application` instead of running a server. This can be useful for
-- installing `Middleware`
distributedVesselEffectApplication
  :: forall es r.
     (Members es r, FromJSON (OneOf r es))
  => (forall a . Sem r a -> Sem DistributedServerCapabilities a)
  -- ^ Effect interpreter
  -> Application
distributedVesselEffectApplication runE req respond = do
  b <- lazyRequestBody req
  let s = requestHeaders req
  let oneOf :: OneOf r es
      Just oneOf = decode b
  case oneOf of
    This e -> do
      let sem = liftSem e
      eRes <- try @SomeException $! join $
        fmap evaluate
        . fmap (\(!h, !v) -> (h, encode v))
        . runM @IO
        . runOutputDeposit
        . runQuery (fromHeaders s)
        $ runE sem
      case eRes of
        Right (responseHeaders, res) ->
          respond $ responseLBS status200 responseHeaders res
        Left e ->
          respond $ responseLBS status500 [] "oopsie"

-- | Like `hostDistributedEffect` except it returns an
-- `Application` instead of running a server. This can be useful for
-- installing `Middleware`
distributedEffectApplication
  :: forall es r.
     (Members es r
     ,FromJSON (OneOf r es)
     )
  => (forall a . Sem r a -> IO a)
  -- ^ Effect interpreter
  -> Application
distributedEffectApplication f = distributedVesselEffectApplication @es @r $ \e ->
  embed $ f e
