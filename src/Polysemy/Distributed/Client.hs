{-|
This module contains functions that handle effects by sending them
over the network to servers.
-}

module Polysemy.Distributed.Client where

import Data.Aeson
import Control.Monad
import Network.HTTP.Types (statusCode)
import Network.HTTP.Client
import Polysemy
import Polysemy.Distributed.Types
import Polysemy.HTTP
import Polysemy.State
import Polysemy.Internal
import Polysemy.Internal.CustomErrors
import Polysemy.Distributed.Vessel
import Polysemy.Alter
import Polysemy.Output
import Polysemy.Error
import qualified Control.Exception as E
import Polysemy.Embed

data DistributedClientError
  = BadResponse
  | InvalidParse
  deriving (Show, Eq, E.Exception)

raiseClientExceptions
  :: Member (Embed IO) r
  => Sem (Error DistributedClientError ': r) a
  -> Sem r a
raiseClientExceptions e = do
  mRes <- runError e
  case mRes of
    Right a -> return a
    Left err -> embed $ E.throwIO err

-- * Distributed clients

-- | Interpret an arbitrary effect `e` on a different machine by
-- making network calls with HTTP. The effect and its returned value
-- will be serialized using JSON
runDistributedEffect
  :: forall e r a.
     (forall m x . ToJSON (e m x)
     ,TransJSON e
     ,FirstOrder e "interpret"
     ,Members '[HTTP, Error DistributedClientError] r
     )
  => String
  -- ^ The full URL (including protocol and port) of the server
  -- which will interpret the effect.
  -> Sem (e ': r) a
  -- ^ The effect `e` containing computation.
  -> Sem r a
  -- ^ The resulting computation, which no longer has the `e` effect
runDistributedEffect nodeURL sem = elimVessel
  . runDistributedVesselEffect @e nodeURL
  $ raiseUnder2 @(Output (Vessel Server)) @(Query (Vessel Client)) sem

-- | Interpret an arbitrary effect `e` on a different machine by
-- making network calls with HTTP. The effect and its returned value
-- will be serialized using JSON.
--
-- Unlike the simpler version `runDistributedEffect`,
-- `runDistributedVesselEffect` gives access to the `Vessel`,
-- which allows effects other than `e` to communicate with each other
-- over the network via HTTP headers. This function will likely pair
-- with `hostDistributedVesselEffect` on the server.
--
-- This version of the function requires two more constraints:
-- `Query` `Vessel` and `Output` `Vessel`.
--
-- * `Query` `Vessel` is required for the interpreter to know which
-- request headers it needs to send allong with the request to
-- interpret `e`.
--
-- * `Output` `Vessel` is required for the interpreter to send
-- information from the response headers back out to the calling of
-- `e` (and above).
--
-- These constraints can usually be discharged with a call to
-- `shareVessel`, which will let consumers + producers of vessel data
-- share the same data.
--
-- >>> import Polysemy.Writer
-- >>> import Polysemy.Observe
-- >>> :set -XAllowAmbiguousTypes
-- >>> :set -fplugin=Polysemy.Plugin
-- >>> :{
--   example :: forall e a .
--              (forall m x . ToJSON (e m x)
--              ,TransJSON e
--              ,FirstOrder e "interpret"
--              )
--           => (forall r . Member e r => Sem r a)
--           -> Sem '[HTTP, Error DistributedClientError] a
--   example =
--     shareVessel
--     . runDistributedVesselEffect @e "http://localhost:80"
-- :}
runDistributedVesselEffect
  :: forall e r a .
     (forall m x . ToJSON (e m x)
     ,TransJSON e
     ,FirstOrder e "interpret"
     ,Members '[ HTTP
               , Query (Vessel Client)
               , Output (Vessel Server)
               , Error DistributedClientError
               ] r
     )
  => String
  -- ^ The full URL (including protocol and port) of the server
  -- which will interpret the effect.
  -> Sem (e ': r) a
  -- ^ The effect `e` containing computation.
  -> Sem r a
  -- ^ The resulting computation, which no longer has the `e` effect
runDistributedVesselEffect nodeURL = interpret @e $ \e -> do
  let Just req = parseRequest nodeURL
  s <- query @(Vessel Client)
  resp <- performRequest req
    { method = "POST"
    , requestBody = RequestBodyLBS $ encode e
    , requestHeaders = toHeaders s
    }
  when (statusCode (responseStatus resp) /= 200) $
    throw BadResponse
  a <- case transFrom e $ decode $ responseBody resp of
    Just a -> return a
    Nothing -> throw InvalidParse
  output . fromHeaders @Server $ responseHeaders resp
  return a
