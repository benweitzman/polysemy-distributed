{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}

module Polysemy.Distributed.Vessel
  (Vessel
  ,VesselAccess(..)
  ,Deposit(..)
  ,VesselPosition(..)
  ,shareVessel
  ,shareDownstreamVessel
  ,insert
  ,find
  ,toHeaders
  ,fromHeaders
  ,runOutputDeposit
  ,elimVessel
  ,eliminateUpstreamVessel
  ,eliminateDownstreamDeposit
  ,shareUpstreamVessel
  ) where

import Data.Aeson
import GHC.TypeLits
import Data.Proxy
import Data.CaseInsensitive (CI)
import qualified Data.CaseInsensitive as CI
import qualified Data.ByteString.Lazy as LBS
import  Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as BS8
import Polysemy.State
import Polysemy
import Network.HTTP.Types (Header)
import Polysemy.Alter
import Polysemy.Output
import Polysemy.Observe
import Polysemy.Writer
import Polysemy.Internal
import Data.Monoid

data VesselPosition = Client | Server

-- | Vessel gives a typed interface to request + respons headers
-- when interpretting effects over the networks
--
-- The type parameter indicates a vessel's source. For example,
-- reading from a `Vessel Server` means reading from data in response
-- headers. Writing to a `Vessel Server` means writing to response headers.
--
-- Some operations may read from a `Vessel Server` /and/ write to a `Vessel Server`.
-- This effectivley positions the operation in the middle of two machines, one
-- upstream and one downstream.
newtype Vessel (p :: VesselPosition) = Vessel [(CI ByteString, ByteString)]
  deriving newtype (Monoid, Semigroup)

-- | A deposit is a single addition to the headers in a `Vessel`.
data Deposit (p :: VesselPosition) where
  Deposit :: (KnownSymbol s, ToJSON a) => Key e s a -> a -> Deposit p

-- | Keys to the Vessel that scope header fields to a particular
-- effect
class VesselAccess (e :: (* -> *) -> * -> *) where
  data family Key e (s :: Symbol) a

toHeaders :: Vessel a -> [Header]
toHeaders (Vessel s) = s

fromHeaders :: [Header] -> Vessel a
fromHeaders = Vessel

insert
  :: forall e s a p . (KnownSymbol s, ToJSON a)
  => Key e s a
  -> a
  -> Vessel p
  -> Vessel p
insert !_ x (Vessel v) =
  let key = CI.mk . BS8.pack . symbolVal $ Proxy @s
      val = LBS.toStrict $ encode x
  in Vessel $ (key, val) : v

find
  :: forall e s a p . (KnownSymbol s, FromJSON a)
  => Key e s a
  -> Vessel p
  -> Maybe a
find !_ (Vessel xs) = do
  x <- lookup (CI.mk . BS8.pack . symbolVal $ Proxy @s) xs
  decode $ LBS.fromStrict x

makeDeposit :: Deposit p -> Vessel p -> Vessel p
makeDeposit (Deposit k v) = insert k v

elimVessel
  :: Sem (Output (Vessel Server) ': Query (Vessel Client) ': r) a
  -> Sem r a
elimVessel = runQuery (Vessel []) . ignoreOutput

shareDownstreamVessel :: Sem (Alter (Deposit p) ': Query (Vessel p) ': r) a -> Sem r a
shareDownstreamVessel = runQualter (Vessel []) makeDeposit

runOutputDeposit :: Sem (Output (Deposit p) ': r) a -> Sem r ([Header], a)
runOutputDeposit m = do
  (Endo s, a) <- flip runOutputMonoid m $ Endo . makeDeposit
  return (toHeaders $ s (Vessel []), a)


eliminateUpstreamVessel
  :: Sem (Observe (Vessel Server) ': r) a
  -> Sem r a
eliminateUpstreamVessel = fmap snd
  . runWriter @(Vessel Server)
  . observeOutputToWriter @(Vessel Server)
  . raiseUnder2

shareUpstreamVessel
  :: Sem (Observe (Vessel Server) ': Output (Vessel Server) ': Writer (Vessel Server) ': r) a
  -> Sem r a
shareUpstreamVessel =
  fmap snd
  . runWriter
  . observeOutputToWriter

shareVessel
  :: Sem (Observe (Vessel 'Server)
          ': Output (Vessel 'Server)
          ': Writer (Vessel 'Server)
          ': Alter (Deposit 'Client)
          ': Query (Vessel 'Client)
          ': r
         ) a
  -> Sem r a
shareVessel = shareDownstreamVessel . shareUpstreamVessel

eliminateDownstreamDeposit
  :: Sem (Output (Deposit Server) ': r) a
  -> Sem r a
eliminateDownstreamDeposit = ignoreOutput
