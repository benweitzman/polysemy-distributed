module Polysemy.Distributed.Types where

import Data.Aeson
import Data.Aeson.Types
import Control.Monad
import Control.Applicative
import Polysemy
import Polysemy.Internal.Union

class TransJSON e where
  transFrom :: e m a -> (FromJSON a => r) -> r
  transTo :: e m a -> (ToJSON a => r) -> r

data Some r e where
  Some :: e (Sem r) a -> Some r e

deriving instance (forall m  a . Show (e m a)) => Show (Some r e)

data ExistentialParser e where
  ExistentialParser :: (forall m . Value -> Parser (e m a)) -> ExistentialParser e

class ParseEffect e where
  effectParsers :: [ExistentialParser e]

parseEffect :: forall r e . ParseEffect e => Value -> Parser (Some r e)
parseEffect val = go effectParsers
  where
    go :: [ExistentialParser e] -> Parser (Some r e)
    go [] = fail "no parse!"
    go (ExistentialParser parser :rest) =
      Some <$> parser val <|> go rest

instance ParseEffect e => FromJSON (Some r e) where
  parseJSON = parseJSON >=> parseEffect

data OneOf s r where
  This :: (ToJSON a) => Union s (Sem s) a -> OneOf s r

instance {-# OVERLAPPING #-}
  ( TransJSON e
  , FromJSON (Some r e)
  , Member e r
  ) => FromJSON (OneOf r '[e]) where

  parseJSON v = do
    Some e <- parseJSON @(Some r e) v
    return $ transTo e $ This $ inj e

instance
  ( FromJSON (OneOf s r)
  , TransJSON e
  , FromJSON (Some s e)
  , Member e s
  ) => FromJSON (OneOf s (e ': r)) where

  parseJSON v = parseE <|> parseR

    where

      parseE :: Parser (OneOf s (e ': r))
      parseE = do
        Some e <- parseJSON @(Some s e) v
        return $ transTo e $ This (inj e)

      parseR :: Parser (OneOf s (e ': r))
      parseR = do
        This u <- parseJSON @(OneOf s r) v
        return $ This u
