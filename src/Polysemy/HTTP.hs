{-# LANGUAGE TemplateHaskell #-}

module Polysemy.HTTP where

import           Data.ByteString.Lazy (ByteString)
import           Network.HTTP.Client
import Polysemy.HTTP.Convert
import           Polysemy
import           Polysemy.Embed
import Network.Wai (Application)
import Network.Wai.Internal (ResponseReceived(..))
import Data.IORef

data HTTP m a where
  PerformRequest :: Request -> HTTP m (Response ByteString)

makeSem ''HTTP

runHTTP :: Member (Embed IO) r => Manager -> Sem (HTTP ': r) a -> Sem r a
runHTTP mgr = interpret $ \case
  PerformRequest req -> embed $ httpLbs req mgr

applicationHTTP :: Member (Embed IO) r => Application -> Sem (HTTP ': r) a -> Sem r a
applicationHTTP app = interpret $ \case
  PerformRequest req -> embed @IO $ do
    ref <- newIORef Nothing
    newReq <- convertRequest req
    app newReq $ \resp -> do
      writeIORef ref (Just resp)
      return ResponseReceived
    Just resp <- readIORef ref
    return $ convertResponse resp
