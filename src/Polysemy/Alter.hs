{-# LANGUAGE TemplateHaskell #-}

module Polysemy.Alter where

import           Polysemy

-- | An effect for querying from a common environment. One half of a
-- `Reader`
data Query i m a where
  -- | Query the environment, analogous to `Ask`
  Query :: Query i m i

makeSem ''Query

-- | Handle `Query` effect by providing the environment in question
runQuery :: i -> Sem (Query i ': r) a -> Sem r a
runQuery i = interpret $ \case
  Query -> return i

-- | An effect for modifiying a common environment locally. One half
-- of a `Reader`
data Alter o m a where
  -- | Alter the environment, analogous to `Local`
  Alter :: o -> m a -> Alter o m a

makeSem ''Alter

-- | Annihilate `Alter` + `Query` simulatenously. Calls to `Alter`
-- will affect calls to `Query`.
runQualter
  :: forall i o r a .
     i -- ^ initial environment
  -> (o -> i -> i) -- ^ modification function
  -> Sem (Alter o ': Query i ': r) a
  -> Sem r a
runQualter i f  = (interpret qq .) $ interpretH aa
  where
    qq :: Query i m x -> Sem r x
    qq Query = return i

    aa :: Alter o m x -> Tactical (Alter o) m (Query i ': r) x
    aa (Alter o m) = do
      m' <- runT m
      raise . raise $ runQualter (f o i) f m'
