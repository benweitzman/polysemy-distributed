{-# LANGUAGE TemplateHaskell #-}

module Polysemy.Observe where

import           Polysemy
import Polysemy.Internal
import Polysemy.Writer
import Polysemy.Output

-- | An effect for observing everything outputted during a computation.
-- One half of a `Writer` (the other half is `Output`)
data Observe o m a where
  Observe :: m a -> Observe o m (o, a)

makeSem ''Observe

-- | Annihilate an `Observe`/`Output` pair of effects into a single
-- `Writer` effect
observeOutputToWriter
  :: forall o r a .
     Member (Writer o) r
  => Sem (Observe o ': Output o ': r) a -- ^
  -> Sem r a
observeOutputToWriter = (interpret qq .) $ interpretH aa
  where
    qq :: Output o m x -> Sem r x
    qq (Output o) = tell o


    aa :: Observe o m x -> Tactical (Observe o) m (Output o ': r) x
    aa (Observe m) = do
      m' <- runT m
      let l = listen @o m'
      (o, fa) <- raise . raise $ observeOutputToWriter l
      return $ (o,) <$> fa
